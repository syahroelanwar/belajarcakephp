<?php
App::uses('AppModel', 'Model');
/**
 * Mahasiswa Model
 *
 */
class Mahasiswa extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'cakebake';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'mahasiswa';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
			'blank' => array(
				'rule' => array('blank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
