<div class="mahasiswas index">
	<h2><?php echo __('Mahasiswas'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nim'); ?></th>
			<th><?php echo $this->Paginator->sort('nama'); ?></th>
			<th><?php echo $this->Paginator->sort('jurusan'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($mahasiswas as $mahasiswa): ?>
	<tr>
		<td><?php echo h($mahasiswa['Mahasiswa']['id']); ?>&nbsp;</td>
		<td><?php echo h($mahasiswa['Mahasiswa']['nim']); ?>&nbsp;</td>
		<td><?php echo h($mahasiswa['Mahasiswa']['nama']); ?>&nbsp;</td>
		<td><?php echo h($mahasiswa['Mahasiswa']['jurusan']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $mahasiswa['Mahasiswa']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $mahasiswa['Mahasiswa']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $mahasiswa['Mahasiswa']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $mahasiswa['Mahasiswa']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Mahasiswa'), array('action' => 'add')); ?></li>
	</ul>
</div>
