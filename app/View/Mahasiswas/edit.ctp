<div class="mahasiswas form">
<?php echo $this->Form->create('Mahasiswa'); ?>
	<fieldset>
		<legend><?php echo __('Edit Mahasiswa'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('nim');
		echo $this->Form->input('nama');
		echo $this->Form->input('jurusan');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Mahasiswa.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Mahasiswa.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Mahasiswas'), array('action' => 'index')); ?></li>
	</ul>
</div>
